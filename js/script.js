// Создайте страницу с кнопкой, при нажатии по кнопке на странице создается div с произвольным текстом. После создания 10 дивов все они должны удалится.

let clicks = 0;
let asd = document.getElementById('section');

const createNewElement = () => {
  let item = document.createElement('div');
  item.innerHTML = 'New div';
  asd.append(item);
};
const button = document.querySelector('input');
button.addEventListener('click', function () {
  console.log(clicks++);
  if (clicks >= 10) {
    let elem = asd.getElementsByTagName('div');
    asd.remove(elem);
    clicks = 0;
  }
});
